# Kitti.py Create dataset for loading the KITTI dataset
# Copyright (C) 2021  Juan Luis Gonzalez Bello (juanluisgb@kaist.ac.kr)
# This software is not for commercial use
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os.path
from .util import split2list
from .listdataset_train import ListDataset as panListDataset
from random import shuffle


def Vaccinium(split, **kwargs):
    input_root = kwargs.pop('root')
    transform = kwargs.pop('transform', None)
    target_transform = kwargs.pop('target_transform', None)
    reference_transform = kwargs.pop('reference_transform', None)
    co_transform = kwargs.pop('co_transform', None)
    shuffle_test_data = kwargs.pop('shuffle_test_data', False)
    max_pix = kwargs.pop('max_pix', 100)
    fix = kwargs.pop('fix', False)

    # From Eigen et. al (NeurIPS 2014)
    with open("Datasets/vaccinium_train.txt", 'r') as f:
        train_list = list(f.read().splitlines())
        train_list = [[line.split(" "), None] for line in train_list if
                      os.path.isfile(os.path.join(input_root, line.split(" ")[0]))]

    with open("Datasets/vaccinium_valid.txt", 'r') as f:
        test_list = list(f.read().splitlines())
        test_list = [[line.split(" "), None] for line in test_list if
                      os.path.isfile(os.path.join(input_root, line.split(" ")[0]))]

    train_dataset = panListDataset(input_root, input_root, train_list, data_name='Vaccinium', disp=False, of=False,
                                   transform=transform, target_transform=target_transform,
                                   co_transform=co_transform,
                                   max_pix=max_pix, reference_transform=reference_transform, fix=fix)
    if shuffle_test_data:
        shuffle(test_list)
    test_dataset = panListDataset(input_root, input_root, test_list, data_name='Vaccinium', disp=False, of=False,
                                  transform=transform, co_transform = co_transform, target_transform=target_transform, fix = fix)
    return train_dataset, test_dataset

