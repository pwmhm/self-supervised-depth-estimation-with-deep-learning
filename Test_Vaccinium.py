# Test_KITTI.py Test trained model on diverse KITTI splits
# Copyright (C) 2021  Juan Luis Gonzalez Bello (juanluisgb@kaist.ac.kr)
# This software is not for commercial use
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import argparse
import time
import numpy as np
from imageio import imsave
import matplotlib.pyplot as plt
from PIL import Image

import Datasets
import models

dataset_names = sorted(name for name in Datasets.__all__)
model_names = sorted(name for name in models.__all__)

parser = argparse.ArgumentParser(description='Testing pan generation',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-d', '--data', metavar='DIR', default='/home/patrick_mfb/Desktop', help='path to dataset')
parser.add_argument('-tn', '--tdataName', metavar='Test Data Set Name', default='Vaccinium_test',
                    choices=dataset_names)
parser.add_argument('-relbase', '--rel_baselne', default=1, type = float, help='Relative baseline of testing dataset')
parser.add_argument('-mdisp', '--max_disp',type=int, default=440)  # of the training patch W
parser.add_argument('-mindisp', '--min_disp',type=int, default=250)  # of the training patch W
parser.add_argument('-b', '--batch_size', metavar='Batch Size', default=1)
parser.add_argument('-eval', '--evaluate', default=True)
parser.add_argument('-save', '--save', default=True)
parser.add_argument('-save_pc', '--save_pc', default=False, type = bool)
parser.add_argument('-save_pan', '--save_pan', default=False)
parser.add_argument('-save_input', '--save_input', default=False)
parser.add_argument('-w', '--workers', metavar='Workers', default=4)
parser.add_argument('--sparse', default=False, action='store_true',
                    help='Depth GT is sparse, automatically seleted when choosing a KITTIdataset')
parser.add_argument('--print-freq', '-p', default=10, type=int, metavar='N', help='print frequency')
parser.add_argument('-gpu_no', '--gpu_no', default='0', help='Select your GPU ID, if you have multiple GPU.')
parser.add_argument('-dt', '--dataset', help='Dataset and training stage directory', default='Vaccinium_stage1')
parser.add_argument('-ts', '--time_stamp', help='Model timestamp', default='07-08-12_46')
parser.add_argument('-m', '--model', help='Model', default='FAL_netC')
parser.add_argument('-no_levels', '--no_levels',type=int, default=10, help='Number of quantization levels in MED')
parser.add_argument('-dtl', '--details', help='details',
                    default=',e50es,b4,lr0.0001/checkpoint.pth.tar')
parser.add_argument('-fpp', '--f_post_process', default=False, help='Post-processing with flipped input')
parser.add_argument('-mspp', '--ms_post_process', default=True, help='Post-processing with multi-scale input')
parser.add_argument('-median', '--median', default=False,
                    help='use median scaling (not needed when training from stereo')
parser.add_argument('-rsz', '--rsz', default=1.0, type=float, help='resize factor')
parser.add_argument('-sample_no', '--sno', default=5, type = int, help='amount of samples to be saved')
parser.add_argument('-adtl', '--adtl', default = None, help = 'additional details')



def display_config(save_path):
    settings = ''
    settings = settings + '############################################################\n'
    settings = settings + '# FAL-net        -         Pytorch implementation          #\n'
    settings = settings + '# by Juan Luis Gonzalez   juanluisgb@kaist.ac.kr           #\n'
    settings = settings + '############################################################\n'
    settings = settings + '-------YOUR TRAINING SETTINGS---------\n'
    for arg in vars(args):
        settings = settings + "%15s: %s\n" % (str(arg), str(getattr(args, arg)))
    print(settings)
    # Save config in txt file
    with open(os.path.join(save_path, 'settings.txt'), 'w+') as f:
        f.write(settings)


def main():
    print('-------Testing on gpu ' + args.gpu_no + '-------')
    print(torch.cuda.is_available())

    save_path = os.path.join('Test_Results', args.tdataName, args.model, args.time_stamp)
    if args.f_post_process:
        save_path = save_path + 'fpp'
    if args.ms_post_process:
        save_path = save_path + 'mspp'
    if args.adtl:
        save_path = save_path + "_" +args.adtl
    print('=> Saving to {}'.format(save_path))

    if not os.path.exists(save_path):
        os.makedirs(save_path)
    display_config(save_path)

    input_transform = transforms.Compose([
        data_transforms.ArrayToTensor(),
        transforms.Normalize(mean=[0, 0, 0], std=[255, 255, 255]),  # (input - mean) / std
        transforms.Normalize(mean=[0.411, 0.432, 0.45], std=[1, 1, 1])
    ])
    co_transform = data_transforms.Compose([
        data_transforms.TestResize(args.rsz)
    ])

    target_transform = transforms.Compose([
        data_transforms.ArrayToTensor(),
        transforms.Normalize(mean=[0], std=[1])
    ])

    # Torch Data Set List
    if args.tdataName == 'Vaccinium_test' :
        input_path = os.path.join(args.data, 'Vaccinium')
    else :
        input_path = os.path.join(args.data, args.tdataName)

    test_dataset, test_filename = Datasets.__dict__[args.tdataName](split=1,  # all to be tested
                                                          root=input_path,
                                                          disp=True,
                                                          shuffle_test=False,
                                                          transform=input_transform,
                                                          co_transform = co_transform,
                                                          target_transform=target_transform)                                           

    # Torch Data Loader
    args.batch_size = 1  # kitty mixes image sizes!
    args.sparse = True  # disparities are sparse (from lidar)
    val_loader = torch.utils.data.DataLoader(test_dataset, batch_size=args.batch_size, num_workers=args.workers,
                                             pin_memory=False, shuffle=False)

    # create pan model
    model_dir = os.path.join(args.dataset, args.time_stamp + "_" + args.model + args.details)
    pan_network_data = torch.load(model_dir)
    pan_model = pan_network_data['m_model']
    print("=> using pre-trained model for pan '{}'".format(pan_model))
    pan_model = models.__dict__[pan_model](pan_network_data, no_levels=args.no_levels).cuda()
    pan_model = torch.nn.DataParallel(pan_model, device_ids=[0]).cuda()
    pan_model.eval()
    model_parameters = utils.get_n_params(pan_model)
    print("=> Number of parameters '{}'".format(model_parameters))
    cudnn.benchmark = True

    # evaluate on validation set
    validate(val_loader, pan_model, save_path, model_parameters, test_filename)


def validate(val_loader, pan_model, save_path, model_param, test_filename):
    global args
    batch_time = utils.AverageMeter()
    EPEs = utils.AverageMeter()
    vac_erros = utils.multiAverageMeter(utils.vaccinium_error_names)

    l_disp_path = os.path.join(save_path, 'l_disp')
    if not os.path.exists(l_disp_path):
        os.makedirs(l_disp_path)

    input_path = os.path.join(save_path, 'Input im')
    if not os.path.exists(input_path):
        os.makedirs(input_path)

    pan_path = os.path.join(save_path, 'Pan')
    if not os.path.exists(pan_path):
        os.makedirs(pan_path)

    pc_path = os.path.join(save_path, 'Point_cloud')
    if not os.path.exists(pc_path):
        os.makedirs(pc_path)

    feats_path = os.path.join(save_path, 'feats')
    if not os.path.exists(feats_path):
        os.makedirs(feats_path)

    gt_path = os.path.join(save_path, 'gt')
    if not os.path.exists(gt_path):
        os.makedirs(gt_path)

    # Set the max disp
    right_shift = args.max_disp * args.rel_baselne

    inference_avg = []
    count_sample = 0
    val_bg = [] #for determining a fixed offset.
    random_avg =[]
    key_bg = { "cal1" : 131, "cal2" : 126, "cal3" : 151}

    with torch.no_grad():
        for i, (input, target, f_name) in enumerate(val_loader):
            #start_rec = torch.cuda.Event(enable_timing=True)
            #end_rec = torch.cuda.Event(enable_timing=True)
            target = target[0].cuda()
            input_left = input[0].cuda()
            input_right = input[1].cuda()

            if args.tdataName == 'Owndata':
                B, C, H, W = input_left.shape
                input_left = input_left[:,:,0:int(0.95*H),:]
                # input_left = F.interpolate(input_left, scale_factor=1.0, mode='bilinear', align_corners=True)
            B, C, H, W = input_left.shape
            
            Size_Ori = (1536, 2048)
            # Prepare flip grid for post-processing
            i_tetha = torch.zeros(B, 2, 3).cuda()
            i_tetha[:, 0, 0] = 1
            i_tetha[:, 1, 1] = 1
            flip_grid = F.affine_grid(i_tetha, [B, C, H, W])
            flip_grid[:, :, :, 0] = -flip_grid[:, :, :, 0]

            # Convert min and max disp to bx1x1 tensors
            max_disp = torch.Tensor([right_shift]).unsqueeze(1).unsqueeze(1).type(input_left.type())
            min_disp = max_disp * args.min_disp / args.max_disp

            # Synthesis
            #torch.cuda.synchronize()
            #start_rec.record()
            end = time.time()

            # Get disp
            if args.save_pan:
                pan_im, disp, maskL, maskRL, dispr = pan_model(input_left, min_disp, max_disp,
                                                        ret_disp=True, ret_subocc=True, ret_pan=True)
                # You can append any feature map to feats, and they will be printed as 1 channel grayscale images
                feats = [maskL, maskRL]
                feats = [local_normalization(input_left), dispr / 100, maskL, maskRL]
            else:
                disp = pan_model(input_left, min_disp, max_disp, ret_disp=True, ret_subocc=False, ret_pan=False)
                feats = None

            if args.f_post_process:
                flip_disp = pan_model(F.grid_sample(input_left, flip_grid), min_disp, max_disp,
                                      ret_disp=True, ret_pan=False, ret_subocc=False)
                flip_disp = F.grid_sample(flip_disp, flip_grid)
                disp = (disp + flip_disp) / 2
            elif args.ms_post_process:
                disp = ms_pp(input_left, pan_model, flip_grid, disp, min_disp, max_disp)

            #disp_raw = disp
            if args.rsz < 1.0 :
                disp = F.interpolate(disp, size=Size_Ori, mode='bilinear', align_corners=True)
                input_left = F.interpolate(input_left, size=Size_Ori, mode='bilinear', align_corners=True)
                #target = F.interpolate(target, size=Size_Ori, mode='bilinear', align_corners=True)
         
            #disp = disp/disp.max()

            batch_time.update(time.time() - end, 1)
            
            
            slice_start=int(disp.shape[3] / 2)
            slice_end=int(disp.shape[3] / 2)+50
            offset_pred = disp.cpu().numpy()[0][0][5][slice_start:slice_end]
            offset_gt = target.cpu().numpy()[0][0][50][slice_start:slice_end]
            mask = offset_gt > 0
            offset_gt = offset_gt[mask]
            
            #offset = np.average(offset_gt) - np.average(offset_pred) #automatic
            offset = key_bg[test_filename[i][1]] - np.average(offset_pred)   #semi automatic
            #offset = 131 - np.average(offset_pred)   #semi automatic
            #offset = -131   
            
            val_bg.append(offset)
            disp = disp + offset
            
            offset_gt_rand = target.cpu().numpy()[0][0][52][slice_start:slice_end]
            mask = offset_gt_rand > 0
            offset_gt_rand = offset_gt_rand[mask]
            offset_pred_rand = disp.cpu().numpy()[0][0][52][slice_start:slice_end]
            offset_rand = np.average(offset_gt_rand) - np.average(offset_pred_rand)
            random_avg.append(np.average(offset_rand))
            
            #plt.subplot(2,1,1)
            #plt.imshow(disp[0][0].cpu().numpy(),cmap="gray", vmin = target[0][0].cpu().numpy().min(), vmax = target[0][0].cpu().numpy().max())
            #plt.imshow(disp[0][0].cpu().numpy(),cmap="gray", vmin = target[0][0].cpu().numpy().min(), vmax = target[0][0].cpu().numpy().max())
            #plt.subplot(2,1,2)
            #plt.imshow(target[0][0].cpu().numpy(), cmap="gray")
            #plt.show()

            # Save outputs to disk
            if args.save and count_sample < args.sno:
                # Save monocular lr
                disparity = disp.squeeze().cpu().numpy()
                disparity = 256 * np.clip(disparity / (np.percentile(disparity, 95) + 1e-6), 0, 1)
                plt.imsave(os.path.join(l_disp_path, '{:010d}.png'.format(i)), np.rint(disparity).astype(np.int32),
                           cmap='gray', vmin=0, vmax=256)
                plt.imsave(os.path.join(l_disp_path, 'alt_{:010d}.png'.format(i)), np.rint(disparity).astype(np.int32),
                           cmap='gray', vmin=disparity.min(), vmax=disparity.max())
                           
                targetiy = target.squeeze().cpu().numpy()
                targetiy = 256 * np.clip(targetiy / (np.percentile(targetiy, 95) + 1e-6), 0, 1)
                plt.imsave(os.path.join(gt_path, '{:010d}.png'.format(i)), np.rint(targetiy).astype(np.int32),
                           cmap='gray', vmin=0, vmax=256)

                if args.save_pc and count_sample < 1:
                    # equalize tone
                    m_rgb = torch.ones((B, C, 1, 1)).cuda()
                    m_rgb[:, 0, :, :] = 0.411 * m_rgb[:, 0, :, :]
                    m_rgb[:, 1, :, :] = 0.432 * m_rgb[:, 1, :, :]
                    m_rgb[:, 2, :, :] = 0.45 * m_rgb[:, 2, :, :]
                    point_cloud = utils.get_point_cloud((input_left + m_rgb) * 255, disp)
                    utils.save_point_cloud(point_cloud.squeeze(0).cpu().numpy(),
                                           os.path.join(pc_path, '{:010d}.ply'.format(i)))
                    point_cloudx = utils.get_point_cloud((input_left + m_rgb) * 255, target)
                    utils.save_point_cloud(point_cloudx.squeeze(0).cpu().numpy(),
                                           os.path.join(pc_path, '{:010d}_gt.ply'.format(i)))                       
                    
                if args.save_input:
                    denormalize = np.array([0.411, 0.432, 0.45])
                    denormalize = denormalize[:, np.newaxis, np.newaxis]
                    p_im = input_left.squeeze().cpu().numpy() + denormalize
                    im = Image.fromarray(np.rint(255 * p_im.transpose(1, 2, 0)).astype(np.uint8))
                    im.save(os.path.join(input_path, '{:010d}.png'.format(i)))

                if args.save_pan:
                    # save synthetic image
                    denormalize = np.array([0.411, 0.432, 0.45])
                    denormalize = denormalize[:, np.newaxis, np.newaxis]
                    im = pan_im.squeeze().cpu().numpy() + denormalize
                    im = Image.fromarray(np.rint(255 * im.transpose(1, 2, 0)).astype(np.uint8))
                    im.save(os.path.join(pan_path, '{:010d}.png'.format(i)))

                # save features per channel as grayscale images
                if feats is not None:
                    for layer in range(len(feats)):
                        _, nc, _, _ = feats[layer].shape
                        for inc in range(nc):
                            mean = torch.abs(feats[layer][:, inc, :, :]).mean()
                            feature = 255 * torch.abs(feats[layer][:, inc, :, :]).squeeze().cpu().numpy()
                            feature[feature < 0] = 0
                            feature[feature > 255] = 255
                            imsave(os.path.join(feats_path, '{:010d}_l{}_c{}.png'.format(i, layer, inc)),
                                   np.rint(feature).astype(np.uint8))
            count_sample = count_sample + 1
                            
            if args.evaluate:
                # Record kitti metrics
                target_disp = target.squeeze(1).cpu().numpy()
                pred_disp = disp.squeeze(1).cpu().numpy()

                #target_depth, pred_depth = utils.disps_to_depths_kitti(target_disp, pred_disp) #considering disabling this
                vac_erros.update( utils.compute_vaccinium_errors(target_disp, pred_disp, factor = args.rsz), target.size(0))
                
            with open(os.path.join('Test_Results', args.tdataName, 'BG_VALUES.csv'), 'a+') as f:
                f.seek(0)
                data = f.read(100)
                if len(data) == 0 :
                    f.write('Image_Index,Offset,Random_Offset,Bg_Value,Cal_Index\n')
                elif len(data) > 0 :
                    f.write('\n')
                f.write('{0},{1},{2},{3},{4}'.format(i, offset, offset_rand, np.average(offset_gt), test_filename[i][1]))

            if i % args.print_freq == 0:
                print('Test: [{0}/{1}]\t Time {2}\t absrel {3:.4f}\t sqrel {4:.4f}\t mse {5:.4f}\t mae {6:.4f}\t mre {7:.4f}\tsze_mean {8:.4f}\t sze {9:.4f}'.format(i, len(val_loader), batch_time, vac_erros.avg[0], vac_erros.avg[1],vac_erros.avg[2],vac_erros.avg[3],vac_erros.avg[4],vac_erros.avg[5],vac_erros.avg[6]))

    # Save erros and number of parameters in txt file
    with open(os.path.join('Test_Results', args.tdataName, 'errors.txt'), 'a+') as f:
        f.seek(0)
        data = f.read(100)
        if len(data) == 0 :
            f.write('Timestamp,abs_rel,sq_rel,mse,mae,mre,sze_mean,sze,conclusion,root,offset,offset_rand\n')
        elif len(data) > 0 :
            f.write('\n')
        f.write('{0},'.format(args.time_stamp))
        f.write('{}'.format(vac_erros))
        f.write('TBD,{0},{1},{2}'.format(save_path, np.mean(val_bg), np.mean(random_avg)))
        
    with open(os.path.join(save_path, 'errors.txt'), 'w+') as f:
        f.write('\nNumber of parameters {}\n'.format(model_param))
        f.write('\nError metrics: \n{}\n'.format(vac_erros))
    if args.evaluate:
        print(vac_erros)
        print(np.mean(val_bg))
        plt.subplot(2,1,1)
        plt.plot(val_bg)
        plt.subplot(2,1,2)
        plt.plot(random_avg)
        save_file = args.time_stamp + "_" + args.model + ".png"
        plt.savefig(os.path.join("Test_Results",args.tdataName,"fig_offset",save_file))


def ms_pp(input_view, pan_model, flip_grid, disp, min_disp, max_pix):
    B, C, H, W = input_view.shape

    up_fac = 2/3
    upscaled = F.interpolate(F.grid_sample(input_view, flip_grid), scale_factor=up_fac, mode='bilinear',
                             align_corners=True)
    dwn_flip_disp = pan_model(upscaled, min_disp, max_pix, ret_disp=True, ret_pan=False, ret_subocc=False)
    dwn_flip_disp = (1 / up_fac) * F.interpolate(dwn_flip_disp, size=(H, W), mode='nearest')#, align_corners=True)
    dwn_flip_disp = F.grid_sample(dwn_flip_disp, flip_grid)

    norm = disp / (np.percentile(disp.detach().cpu().numpy(), 95) + 1e-6)
    norm[norm > 1] = 1

    return (1 - norm) * disp + norm * dwn_flip_disp


def local_normalization(img, win=3):
    B,C,H,W = img.shape
    mean = [0.411, 0.432, 0.45]
    m_rgb = torch.ones((B, C, 1, 1)).type(img.type())
    m_rgb[:, 0, :, :] = mean[0] * m_rgb[:, 0, :, :]
    m_rgb[:, 1, :, :] = mean[1] * m_rgb[:, 1, :, :]
    m_rgb[:, 2, :, :] = mean[2] * m_rgb[:, 2, :, :]

    img = img + m_rgb
    img = img.cpu()

    # Get mean and normalize
    win_mean_T = F.avg_pool2d(img, kernel_size=win, stride=1, padding=(win-1)//2) # B,C,H,W
    win_std = F.avg_pool2d((img - win_mean_T)**2, kernel_size=win, stride=1, padding=(win-1)//2) ** (1/2)
    win_norm_img = (img - win_mean_T) / (win_std + 0.0000001)
    # win_norm_img = win_std

    # padded_img = F.pad(img.clone(), [(win-1)//2, (win-1)//2, (win-1)//2, (win-1)//2], mode='reflect')
    # for i in range(win):
    #     for j in range(win):
    #         if i == 0 and j == 0:
    #             img_ngb = padded_img[:,:,i:H + i, j:W + j].unsqueeze(1)
    #         else:
    #             img_ngb = torch.cat((img_ngb, padded_img[:, :, i:H + i, j:W + j].unsqueeze(1)), 1) # B,win**2,C,H,W
    #
    # # Reshape for matrix multiplications
    # img_ngb = img_ngb.view((B, win**2, C, H * W))
    # img_ngb = torch.transpose(torch.transpose(img_ngb, 1, 3), 2, 3)
    # img_ngb = img_ngb.view((B * H * W, win**2, C))
    #
    # win_mean = win_mean_T.clone().view((B, C, H * W))
    # win_mean = torch.transpose(win_mean, 1, 2)
    # win_mean = win_mean.view((B * H * W, C)).unsqueeze(2)
    #
    # # Get inverse variance matrix
    # # win_var=inv(winI'*winI/neb_size-win_mu*win_mu' +epsilon/neb_size*eye(c));
    # eye = torch.eye(win).type(img_ngb.type())
    # eye = eye.reshape((1, win, win))
    # eye = eye.repeat(B, 1, 1)
    # eps = 0.000001 / (win**2)
    # win_var_inv = torch.inverse(img_ngb.transpose(1,2).bmm(img_ngb) / (win**2) - win_mean.bmm(win_mean.transpose(1,2))
    #                         + eps * eye) #3x3 matrices
    #
    # # Remove the mean and multiply by variance
    # win_norm_img = win_norm_img.view((B, C, H * W))
    # win_norm_img = torch.transpose(win_norm_img, 1, 2)
    # win_norm_img = win_norm_img.view((B * H * W, C)).unsqueeze(1)
    # win_norm_img = win_norm_img.bmm(win_var_inv).squeeze(1) # B*H*W, C, 1
    #
    # # Reshape in B,C,H,W format
    # win_norm_img = win_norm_img.view((B, H * W, C)).transpose(1, 2)
    # win_norm_img = win_norm_img.view((B, C, H, W))

    return win_norm_img


if __name__ == '__main__':
    import os

    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_no

    import torch
    import torch.utils.data
    import torch.nn.parallel
    import torch.backends.cudnn as cudnn
    import torchvision.transforms as transforms
    import torch.nn.functional as F

    # Usefull tensorboard call
    # tensorboard --logdir=C:ProjectDir/NeurIPS2020_FAL_net/Kitti --port=6012

    import myUtils as utils
    import data_transforms
    from loss_functions import realEPE

    args = parser.parse_args()

    main()
