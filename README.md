# Self Supervised Depth Estimation with Deep Learning - Projektarbeit

This project explores the implementation of the model "FAL-Net" from a paper titled "Forget About the LiDAR: Self-Supervised Depth Estimators with MED Probability Volumes" authored by Gonzalez et al. Detailed description regarding this model can be found in the original repository https://github.com/JuanLuisGonzalez/FAL_net .

# How to use

### Preparing the dataset
The model uses stereo images, and it assumes that the images are calibrated and rectified. You can use [this project](https://gitlab.com/pwmhm/camera-calibration) to do that. In addition to that, the directory of the dataset should be :

    Dataset_Directory
      | Left
          | images.png
      | Right
          | images.png
      | Depth
          | images.png

### Training
There are two stages of training. To train the network with "Vaccinium" dataset, the following command can be used :

    python Train_Stage1_K.py --pretrained "" -maxd 440 -mind 300 -b 4 -ch 400 -cw 600 --epochs 50 --milestones 30 40  -n0 "Vaccinium"

Additionaly, after training the model, a fine-tuning second stage can be used. To do this, the following command can be used :

    python Train_Stage2_K.py --pretrained "<insert 1st stage model here>" -maxd 440 -mind 300 -b 2 -ch 400 -cw 600 --epochs 20 --milestones 5 10  -n0 "Vaccinium"

### Testing 
To test the model on Vaccinium, the script Vaccinium_Test.py can be used. An example of the command, with the important params, is as follows :

    python Test_Vaccinium.py --max_disp 440 --min_disp 300 --dataset "Vaccinium_stage1" --no_levels 49 --model "FAL_netB" --time_stamp "10-11-00_52" --details ",e50es,b6,lr0.0001/model_best.pth.tar" --sno 5;

All these parameters, starting from --max_disp all the way to --details have to be the same as the details of the network being tested, which can be viewed in the configuration file of each network, located in (//dataset//Timestamp,Details//settings.txt).

The best result is available [here](https://seafile.zfn.uni-bremen.de/f/6460e04f82ad4a73bb3d/). To reproduce the result, please use this code :

    python Test_Vaccinium.py --max_disp 440 --min_disp 300 --dataset "Vaccinium_stage1" --no_levels 49 --model "FAL_netB" --time_stamp "08-20_00_04" --details ",e25es,b4,lr0.0001/model_best.pth.tar" --sno 5;


